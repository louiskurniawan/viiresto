import React, { Component } from "react";
import { Provider }         from "react-redux";
import { StyleProvider }    from "native-base";
import {
  Spinner,
} from "native-base";
import App              from "../App";
import configureStore   from "./configureStore";
import getTheme         from "../theme/components";
import variables        from "../theme/variables/commonColor";
import { PersistGate }  from "redux-persist/es/integration/react";

class Setup extends Component {
  state: {
    store: Object,
    isLoading: boolean
  };
  constructor() {
    super();
    this.state = {
      isLoading: false,
      persistor : configureStore().persistor,
      store: configureStore(() => this.setState({ isLoading: false })).store
    };
  }
  render() {
    return (
      <StyleProvider style={getTheme(variables)}>
         <Provider store={this.state.store}>
            <PersistGate loading={<Spinner />} persistor={this.state.persistor}>
              <App />
            </PersistGate>
         </Provider>
      </StyleProvider>
    );
  }
}
export default Setup;
