import config from "../config.js";
import { Alert } from "react-native";

export default (uri : string, method : string, body : string) => {
    const BASE_URL = `${config.baseURL}api/cms_team/list`;
    const fetchBody = body ? body : null;
    const fetchMethod =  method ? method : "GET";

    return refreshToken().then(([response, json]) => {
        if (response.status === 200) {
            const fetchHeader = {
                Accept : "application/json",
                Authorization : `${json.token_type} ${json.access_token}`,
                "Content-Type" : "application/json",
            };
            return fetch( BASE_URL, {
                method : fetchMethod,
                headers : fetchHeader,
                body : fetchBody,
            }).then( fetchResponse => {
                return fetchResponse.status === 405
                    ? Alert.alert("Make sure the request method is match with the Api")
                    : Promise.all([fetchResponse, fetchResponse.json()]);
            });
        } else {
            Alert.alert("Oops, something went wrong");
        }
    });
};

async function refreshToken(){
    try {
        const BASE_URL = `${config.baseURL}oauth/token`;
        const header = {
            Accept : "application/json",
             "Content-Type": "application/json"
        };
        const body = JSON.stringify({
            grant_type: config.grant_type,
            client_id: config.client_id,
            client_secret: config.client_secret,
            username: config.username,
            password: config.password,
        });
        let response = await fetch(BASE_URL, {
            method : "POST",
            headers : header,
            body : body,
        })
        return Promise.all([response, response.json()]);
    } catch (e){
        alert(e)
    }
}
