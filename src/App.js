// @flow
import React, {Component} from "react";
import {
   StackNavigator,
   DrawerNavigator
} from "react-navigation";
import { Root }         from "native-base";

import Login            from "./screens/Login/";
import ForgotPassword   from "./screens/ForgotPassword";
import SignUp           from "./screens/SignUp/";
import Walkthrough      from "./screens/Walkthrough/";
import Comments         from "./screens/Comments/";
import Channel          from "./screens/Channel";
import Story            from "./screens/Story";
import Home             from "./screens/Home/";
import Channels         from "./screens/Channels";
import Sidebar          from "./screens/Sidebar";
import Overview         from "./screens/Overview";
import Calendar         from "./screens/Calendar/";
import Timeline         from "./screens/Timeline";
import Feedback         from "./screens/Feedback/";
import Profile          from "./screens/Profile/";
import Settings         from "./screens/Settings";
/*___________________________________________________*/

import Team             from "./screens/Team";
import Main             from "./screens/Main";
import Services         from "./screens/Services";
import News             from "./screens/News";

const Drawer = DrawerNavigator(
  {
    Main    : { screen: Main },
    Team    : { screen: Team },
    Services: { screen: Services },
    News    : { screen: News },
    Home    : { screen: Home },
    Channels: { screen: Channels },
    Overview: { screen: Overview },
    Calendar: { screen: Calendar },
    Timeline: { screen: Timeline },
    Feedback: { screen: Feedback },
    Profile : { screen: Profile  },
    Settings: { screen: Settings },
  },
  {
    initialRouteName: "Team",
    contentComponent: props => <Sidebar {...props} />
  }
);

const App = StackNavigator(
  {
    Login         : { screen: Login },
    SignUp        : { screen: SignUp },
    ForgotPassword: { screen: ForgotPassword },
    Walkthrough   : { screen: Walkthrough },
    Story         : { screen: Story },
    Comments      : { screen: Comments },
    Channel       : { screen: Channel },
    Drawer        : { screen: Drawer }
  },
  {
    index: 0,
    initialRouteName: "Drawer",
    headerMode: "none"
  }
);

class RootNavigator extends Component{
  render(){
    return (
      <Root>
        <App />
      </Root>
    );
  }
}
export default RootNavigator;
