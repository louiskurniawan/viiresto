const React = require("react-native");

const { Dimensions, Platform } = React;

const primary = require("../../theme/variables/commonColor").brandPrimary;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    width: null,
    height: null,
    backgroundColor:'whitesmoke'
  },
  newsHeader: {
    color: "#A9A9A9",
    fontWeight: "500",
    fontSize: 14,
  },
  imageHeader: {
    height: 25,
    width: 95,
    resizeMode: "contain"
  },
  list : {
    justifyContent : 'space-between',
    alignItems : 'center',
    paddingVertical:20,
    flexWrap:'wrap'
  },
  titleHeader:{
    color:'white'
  },
  centerText : {
    textAlign : 'center'
  },
  box:{
    marginVertical : 20,
    alignItems:'center',
    width  : deviceWidth/4,
  },
  primaryColor:{
    color:primary
  },
  desc : {
    color:'lightsteelblue',
  },
};
