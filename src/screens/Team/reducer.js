const initialState = {
    isLoading : true,
    hasErrored : false,
    teams : []
};
export default (state: any = initialState, action: Function) =>  {
    switch (action.type) {
        case "FETCH_TEAM_ERRORED" :
            return {...state, hasErrored: action.hasErrored};
        case "TEAM_IS_LOADING" :
            return {...state, isLoading: action.isLoading };
        case "FETCH_TEAM_SUCCESS" : 
            return {...state, teams : action.teams};
        default :
            return state;
    }
};
