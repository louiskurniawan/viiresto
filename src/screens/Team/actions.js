import fetchRequest from "../../service/fetchRequest";

export function errorFetchTeam(bool : boolean) {
    return {
        type : "FETCH_TEAM_ERRORED",
        hasErrored : bool
    };
}
export function loadingFetchTeam(bool : boolean){
    return {
        type : "TEAM_IS_LOADING",
        isLoading : bool
    };
}
export function successFetchTeam(teams : Object){
    return {
        type : "FETCH_TEAM_SUCCESS",
        teams
    };
}
export function fetchTeam() {
    return dispatch => {
        dispatch(loadingFetchTeam(false));
        return fetchRequest("api/cms_team/list", "POST").then(([response, json]) => {
            response.status === 200
                ? dispatch(successFetchTeam(json))
                : dispatch(errorFetchTeam(false));
        });
    };
}