const data = [
  {
    id : 1,
    feature : "About",
  },
  {
    id : 2,
    feature : "Services",
  },
  {
    id : 3,
    feature : "News",
  },
  {
    id : 4,
    feature : "Team",
  },
  {
    id : 5,
    feature : "Portfolio",
  },
  {
    id : 6,
    feature : "Location",
  },
]
export default data;
