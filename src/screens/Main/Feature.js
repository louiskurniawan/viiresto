import React from 'react';
import {
  Container,
  Header,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  View,
  Spinner,
  Thumbnail,
} from "native-base";
import styles           from "./styles";
import {Grid, Col, Row} from "react-native-easy-grid";
const primary = require("../../theme/variables/commonColor").brandPrimary;

const Feature = (props) => {
  const { feature, ...other } = props
  const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";

  return(
    <View style={styles.box}>
      <Thumbnail large style={{backgroundColor:primary}}/>
      <Text style={{color:'black', alignSelf:'center'}}>{ feature }</Text>
    </View>
  );
};

export default Feature;
